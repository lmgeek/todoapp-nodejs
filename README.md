# ionic-api-client


### Pre-requisitos 📋
```
git clone https://gitlab.com/lmgeek/ionic-api-client/
```

### Instalar el proyecto 📦
_npm leerá el archivo package.json para ver que dependencias tiene el proyecto y las descargará e instalará en la carpeta node-modules._
```
npm install 
```

### Levantamos el servidor 🚀
_Con esto logramos transpilar el código TypeScript y correrá la aplicación por primera vez en nuestro navegador._
```
node index.js
```




---
⌨️ con ❤️ por [Luis Marin](https://gitlab.com/lmgeek) 😊
